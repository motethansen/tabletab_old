require 'spec_helper'

describe "tables/new" do
  before(:each) do
    assign(:table, stub_model(Table,
      :tablenumber => "MyString",
      :tablename => "MyString"
    ).as_new_record)
  end

  it "renders new table form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => tables_path, :method => "post" do
      assert_select "input#table_tablenumber", :name => "table[tablenumber]"
      assert_select "input#table_tablename", :name => "table[tablename]"
    end
  end
end
