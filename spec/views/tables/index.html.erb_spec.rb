require 'spec_helper'

describe "tables/index" do
  before(:each) do
    assign(:tables, [
      stub_model(Table,
        :tablenumber => "Tablenumber",
        :tablename => "Tablename"
      ),
      stub_model(Table,
        :tablenumber => "Tablenumber",
        :tablename => "Tablename"
      )
    ])
  end

  it "renders a list of tables" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Tablenumber".to_s, :count => 2
    assert_select "tr>td", :text => "Tablename".to_s, :count => 2
  end
end
