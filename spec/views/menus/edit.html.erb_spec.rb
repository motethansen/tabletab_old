require 'spec_helper'

describe "menus/edit" do
  before(:each) do
    @menu = assign(:menu, stub_model(Menu,
      :title => "MyString",
      :description => "MyString",
      :category => "MyString",
      :price => "9.99",
      :picture => "MyString",
      :happyhourprice => "9.99"
    ))
  end

  it "renders the edit menu form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => menus_path(@menu), :method => "post" do
      assert_select "input#menu_title", :name => "menu[title]"
      assert_select "input#menu_description", :name => "menu[description]"
      assert_select "input#menu_category", :name => "menu[category]"
      assert_select "input#menu_price", :name => "menu[price]"
      assert_select "input#menu_picture", :name => "menu[picture]"
      assert_select "input#menu_happyhourprice", :name => "menu[happyhourprice]"
    end
  end
end
