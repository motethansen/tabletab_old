require 'spec_helper'

describe "menus/index" do
  before(:each) do
    assign(:menus, [
      stub_model(Menu,
        :title => "Title",
        :description => "Description",
        :category => "Category",
        :price => "9.99",
        :picture => "Picture",
        :happyhourprice => "9.99"
      ),
      stub_model(Menu,
        :title => "Title",
        :description => "Description",
        :category => "Category",
        :price => "9.99",
        :picture => "Picture",
        :happyhourprice => "9.99"
      )
    ])
  end

  it "renders a list of menus" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => "Category".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "Picture".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
  end
end
