require 'spec_helper'

describe "menus/show" do
  before(:each) do
    @menu = assign(:menu, stub_model(Menu,
      :title => "Title",
      :description => "Description",
      :category => "Category",
      :price => "9.99",
      :picture => "Picture",
      :happyhourprice => "9.99"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Title/)
    rendered.should match(/Description/)
    rendered.should match(/Category/)
    rendered.should match(/9.99/)
    rendered.should match(/Picture/)
    rendered.should match(/9.99/)
  end
end
