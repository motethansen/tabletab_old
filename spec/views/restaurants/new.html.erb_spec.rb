require 'spec_helper'

describe "restaurants/new" do
  before(:each) do
    assign(:restaurant, stub_model(Restaurant,
      :name => "MyString",
      :street => "MyString",
      :city => "MyString",
      :state => "MyString",
      :zip => "MyString",
      :country => "MyString",
      :phone => "MyString",
      :restaurantSize => "MyString",
      :numberOfStaffPrShift => 1,
      :restaurantType => "MyString",
      :franchise => false
    ).as_new_record)
  end

  it "renders new restaurant form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => restaurants_path, :method => "post" do
      assert_select "input#restaurant_name", :name => "restaurant[name]"
      assert_select "input#restaurant_street", :name => "restaurant[street]"
      assert_select "input#restaurant_city", :name => "restaurant[city]"
      assert_select "input#restaurant_state", :name => "restaurant[state]"
      assert_select "input#restaurant_zip", :name => "restaurant[zip]"
      assert_select "input#restaurant_country", :name => "restaurant[country]"
      assert_select "input#restaurant_phone", :name => "restaurant[phone]"
      assert_select "input#restaurant_restaurantSize", :name => "restaurant[restaurantSize]"
      assert_select "input#restaurant_numberOfStaffPrShift", :name => "restaurant[numberOfStaffPrShift]"
      assert_select "input#restaurant_restaurantType", :name => "restaurant[restaurantType]"
      assert_select "input#restaurant_franchise", :name => "restaurant[franchise]"
    end
  end
end
