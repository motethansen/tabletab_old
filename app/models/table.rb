class Table < ActiveRecord::Base
  attr_accessible :tablename, :tablenumber, :restaurant_id
  
  belongs_to :restaurants
end
