class Orders < ActiveRecord::Base
  attr_accessible :create_timestamp, :deliver_timestamp, :end_prepare_timestamp, :restaurant_id, :start_prepare_timestamp, :status, :table_id

  has_many :OrderItemlist;
  accepts_nested_attributes_for :OrderItemlist, :reject_if => lambda { |a| a[:content].blank? }, :allow_destroy => true
  
end
