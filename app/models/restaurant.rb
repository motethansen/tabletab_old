class Restaurant < ActiveRecord::Base
  attr_accessible :city, :country, :franchise, :name, :numberOfStaffPrShift, :phone, :restaurantSize, 
                  :restaurantType, :state, :street, :zip, :user_id

  belongs_to :user
  validates :user_id, presence: true
  has_many :tables, dependent: :destroy
  has_many :menus, dependent: :destroy
end
