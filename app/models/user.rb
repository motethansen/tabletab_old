class User < ActiveRecord::Base
  attr_accessible :name, :phone, :email, :password, :password_confirmation, :activeRestaurant_id
  
  attr_accessor :password
  before_save :encrypt_password
  # before_save { user.email = email.downcase }
  
  validates_length_of :email, :within => 3..40, :allow_blank => true
  validates_length_of :password, :within => 5..40, :allow_blank => true
  
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :email, presence:   true,
                      format:     { with: VALID_EMAIL_REGEX },
                      uniqueness: { case_sensitive: false }
  validates_confirmation_of :password
  validates_presence_of :email, :password, :on => :create, :allow_blank => true, :message => "cannot be blank"
  validates_uniqueness_of :email
  validates_format_of :email, :with => /^[^ ]+$/
  
  has_many :restaurants, dependent: :destroy

  
  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end
  
  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end
end