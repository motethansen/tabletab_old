class Menu < ActiveRecord::Base
  attr_accessible :category, :description, :happyhourfrom, :happyhourprice, :happyhourto,  
  :image, :remote_image_url, :picture, :price, :title, :restaurant_id, :displayonmobilemenu_id


  mount_uploader :image, ImageUploader
  
  validates :category, :title, :price, presence: true
  belongs_to :restaurants
end
