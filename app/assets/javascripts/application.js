// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require_tree .
//= require gritter
//= require rails.validations



	function notifyScript() {     
			var evt = document.createEvent("Event");     	
			evt.initEvent("notify", false, false);     
			if (document.getElementById("addthis-extension-script") == null) {        
				var d=document.createElement("div"); d.setAttribute("style", "display:none"); 
				d.setAttribute("id", "addthis-extension-script");         
				if (window._ate)            
				 d.textContent=_ate.pub();        
				 else if(window.addthis_config && addthis_config.pubid)   
				 d.textContent= addthis_config.pubid;        
				else if(window.addthis_config && addthis_config.username)    
				d.textContent= addthis_config.username;        
				else if(window.addthis_pub)    d.textContent= addthis_pub;        
				else            
				 d.textContent="";         
				document.body.appendChild(d);    
			 }    
			document.documentElement.dispatchEvent(evt); }notifyScript()

   		
			  var videoElement = document.getElementById("tabletabPage");

			  function toggleFullScreen() {
			    if (!document.mozFullScreen && !document.webkitFullScreen) {
			      if (videoElement.mozRequestFullScreen) {
			        videoElement.mozRequestFullScreen();
			      } else {
			        videoElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
			      }
			    } else {
			      if (document.mozCancelFullScreen) {
			        document.mozCancelFullScreen();
			      } else {
			        document.webkitCancelFullScreen();
			      }
			    }
			  }

			  document.addEventListener("keydown", function(e) {
			    if (e.keyCode == 13) {
			      toggleFullScreen();
			    }
			  }, false);
		