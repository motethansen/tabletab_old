class OrdersController < ApplicationController

  def show
      @table = Table.find(params[:table_id])
      @menu = Menu.where(:restaurant_id => @table.restaurant_id, :displayonmobilemenu_id => true ).all
      render "orders"
  end

  def place_order
    
    @menuItem = Menu.find(params[:menu_id])
    @orderitem = OrderItemlist.new(params[:menu_item_id])
    
    #redirect_to order_path(@order)
  end
  
  def new
       @order = Orders.new
  end
  
  def create
    @order = Orders.new(params[:table_id])
    if @order.save
      # flash[:success] = "Welcome to TABLETAB "
      #redirect_to root_url, :notice => "Signed up!" 
      #redirect_to @user
      gflash :notice => { :value => "The Order was saved in our database !", :time => 4000 } 
    else
      render "orders"
    end
  end
  
end
