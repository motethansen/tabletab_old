# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121010091604) do

  create_table "menus", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.string   "category"
    t.decimal  "price",                  :precision => 8, :scale => 2
    t.time     "happyhourfrom"
    t.time     "happyhourto"
    t.decimal  "happyhourprice",         :precision => 8, :scale => 2
    t.datetime "created_at",                                           :null => false
    t.datetime "updated_at",                                           :null => false
    t.string   "image"
    t.integer  "restaurant_id"
    t.boolean  "displayonmobilemenu_id"
  end

  create_table "order_itemlists", :force => true do |t|
    t.integer  "menu_item_id"
    t.integer  "order_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "orders", :force => true do |t|
    t.integer  "restaurant_id"
    t.integer  "table_id"
    t.string   "status"
    t.datetime "create_timestamp"
    t.datetime "start_prepare_timestamp"
    t.datetime "end_prepare_timestamp"
    t.datetime "deliver_timestamp"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "restaurants", :force => true do |t|
    t.string   "name"
    t.string   "street"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "country"
    t.string   "phone"
    t.string   "restaurantSize"
    t.integer  "numberOfStaffPrShift"
    t.string   "restaurantType"
    t.boolean  "franchise"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.integer  "user_id"
  end

  create_table "tables", :force => true do |t|
    t.string   "tablenumber"
    t.string   "tablename"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "restaurant_id"
  end

  create_table "users", :force => true do |t|
    t.string   "email"
    t.string   "password_hash"
    t.string   "password_salt"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "name"
    t.string   "phone"
    t.integer  "activeRestaurant_id"
  end

end
