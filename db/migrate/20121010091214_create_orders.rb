class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :restaurant_id
      t.integer :table_id
      t.string :status
      t.datetime :create_timestamp
      t.datetime :start_prepare_timestamp
      t.datetime :end_prepare_timestamp
      t.datetime :deliver_timestamp

      t.timestamps
    end
  end
end
