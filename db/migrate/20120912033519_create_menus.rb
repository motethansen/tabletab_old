class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.string :title
      t.text :description
      t.string :category
      t.decimal :price, precision: 8, scale: 2
      t.string :picture
      t.time :happyhourfrom
      t.time :happyhourto
      t.decimal :happyhourprice, precision: 8, scale: 2

      t.timestamps
    end
  end
end
