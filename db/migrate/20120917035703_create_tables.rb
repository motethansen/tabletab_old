class CreateTables < ActiveRecord::Migration
  def change
    create_table :tables do |t|
      t.string :tablenumber
      t.string :tablename

      t.timestamps
    end
  end
end
