class CreateRestaurants < ActiveRecord::Migration
  def change
    create_table :restaurants do |t|
      t.string :name
      t.string :street
      t.string :city
      t.string :state
      t.string :zip
      t.string :country
      t.string :phone
      t.string :restaurantSize
      t.integer :numberOfStaffPrShift
      t.string :restaurantType
      t.boolean :franchise

      t.timestamps
    end
  end
end
