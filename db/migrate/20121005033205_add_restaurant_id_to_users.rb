class AddRestaurantIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :activeRestaurant_id, :integer
  end
end
